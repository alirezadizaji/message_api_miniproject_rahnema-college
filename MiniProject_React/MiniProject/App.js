/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, TextInput, Button} from 'react-native';
import axios from 'axios';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

class NameText extends React.Component {
  render() {
    return (
      <TextInput
      {...this.props}
      editable = {true}
      maxLength = {20}
      style={{fontSize: 22}}
      />

    )
  }
}

class MessageText extends React.Component {
  render() {
    return (
      <TextInput
      {...this.props}
      editable = {true}
      maxLength = {40}
      style={{fontSize: 22}}
  />);
  }
}

type Props = {};
export default class App extends Component<props> {
  constructor(props) {
    super(props);
    this.state = {users:[], name: "", message: "", date: "2"}
    this.onPressHandler = this.onPressHandler.bind(this)
    this.onPressHandler2 = this.onPressHandler2.bind(this)
  }

  onPressHandler2 = event => {
    event.preventDefault();
    axios.get('http://192.168.1.7:8080/home/all').then(res => {
     this.setState({users: res.data});
    });
  }

  

  onPressHandler = event => {
    event.preventDefault();
    if(this.state.name == 0 || this.state.message == 0)
    return;
    const newItem = {name: this.state.name, message: this.state.message}
      const user = JSON.stringify({
        name: this.state.name,
        message: this.state.message,
        date: this.state.date
      })

      let headers = {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }

    }
      axios.post('http://192.168.1.7:8080/home/register', user, headers);
  }

  render() {
    return (
      <View >
        <Text style = {{fontSize: 20}}>your name: </Text>
        <View style = {{borderColor: "grey",
        borderWidth: 3,
        }
      }>
          <NameText 
          multiline = {false}
           numberOfLines = {1}
          onChangeText = {(text) => this.setState({name: text})}
          value = {this.state.name}
           />
        </View>
        <Text style = {{fontSize: 20}}>your message: </Text>
        <View style = {{borderColor: "grey",
        borderWidth: 3,
        }
      }>
          <MessageText
           multiline = {true}
          numberOfLines = {4}
          onChangeText = {(text) => this.setState({message: text})}
          value = {this.state.message}
           />
        </View>


        <Button
        title = {"register"}
        onPress = {this.onPressHandler}
        />
        <Button
        title = {"showall"}
        onPress = {this.onPressHandler2}
        />

<Text>states: {this.state.users.map(user => user.name)}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
