package Controller;

import Model.User;
import Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
@RequestMapping("/home")
public class UserController {


    @Autowired
    private UserService service;


    @RequestMapping(method = RequestMethod.GET, value = "/all")
    public ArrayList<User> allUsers() {
        return service.allUsers();
    }

    @RequestMapping(method = RequestMethod.POST, value = "/register")
    public void addUser(@RequestBody User user) {
        service.addUser(user);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/delete")
    public void deleteUser() {
        service.deleteUsers();
    }
}
