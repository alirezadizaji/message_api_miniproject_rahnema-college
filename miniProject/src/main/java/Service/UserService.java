package Service;

import Model.User;
import Repository.UserRespository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class UserService {

    @Autowired
    private UserRespository userRespository;

    public ArrayList<User> allUsers() {
        ArrayList<User> users = new ArrayList<>(userRespository.findAll());
        return users;
    }

    public void addUser(User user) {
        userRespository.save(user);
    }

    public void deleteUsers() {
        userRespository.deleteAll();
    }
}
